package com.olli.project.app;

import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import javax.swing.SwingUtilities;

public class ShowListFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -319660389655850692L;
	
	JFrame self = this;
	JFrame frame = new JFrame();
	
	
	
	private JButton backBtn;
	
	/* Constructor */
	public ShowListFrame() throws SQLException{
		
		createPaneAndContent();
	}
	
	
	
	private JComponent createPaneAndContent(){
		JScrollPane pane = new JScrollPane();
		
		JPanel panel = new JPanel(new GridLayout(3, 0));

		

		//Back(Button)
		backBtn = new JButton("Back to list view");
		panel.add(backBtn);

		backBtn.addActionListener(new ActionListener() { 
			
		  public void actionPerformed(ActionEvent e) { 
		    reloadAllListsFrame();
		  } 
		} );
		
		add(panel);
		
		return pane;
	}
	
	public void reloadAllListsFrame(){
		SwingUtilities.invokeLater( new Runnable(){			
			@Override
			public void run(){
				AllListsFrame allListsFrame;
				try {
					allListsFrame = new AllListsFrame();
					allListsFrame.setSize(600, 800);
					allListsFrame.setLocationRelativeTo(null);
					allListsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

					
					/* Destroying all  windows before setting up new*/
					System.gc();
					for (Window window : Window.getWindows()) {
					    window.dispose();
					}
					
					
					allListsFrame.setVisible(true); //setting the AllListsFrame class visible
					
			
					
					

				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		});
		}
}

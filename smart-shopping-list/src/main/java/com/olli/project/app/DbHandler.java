package com.olli.project.app;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import com.googlecode.flyway.core.Flyway;

public class DbHandler {
	
 private static Logger LOGGER = Logger.getLogger(DbHandler.class.getName());


  private static final DbHandler INSTANCE = new DbHandler();
  
  
  public static DbHandler getInstance(){
	  return DbHandler.INSTANCE;
  }
  
  private BasicDataSource ds = new BasicDataSource();
  
  private DbHandler(){	  
  }
  
  public DataSource getDataSource(){
	  return ds;
  }
  
  public void init() throws SecurityException, IOException{
	  FileHandler fh = new FileHandler("shoppinglist.log"); // For database logging purposes
		
		//Attaching Handler to the Logger
		LOGGER.addHandler(fh);
		// Passing all details to the logger
		Logger.getLogger(App.class.getName()).setLevel(Level.ALL);
		
		final Properties properties = new Properties();
		LOGGER.log( Level.FINE, "Creating the datasource");
	//	properties.setDriverClassName("org.h2.Driver");
		properties.put("db.path", "data/db");
		properties.put("db.username", "root");
		properties.put("db.password", "");
		try{
			properties.load(getClass().getResourceAsStream("/app.properties"));
		} catch (final IOException e){
			LOGGER.log(Level.SEVERE, "Failed to load properties");
		}
		
		ds = new BasicDataSource();
		ds.setDriverClassName("org.h2.Driver");
		ds.setUrl("jdbc:h2:" + properties.getProperty("db.path"));
		ds.setUsername(properties.getProperty("db.username"));
		ds.setPassword(properties.getProperty("db.password"));
		
		LOGGER.log(Level.INFO, "Executing Flyway (Migrating database)");
		Flyway fw = new Flyway();
		fw.setDataSource(ds);;
		fw.migrate();
	  
  }
  


	
	  public void close(){
		  
		  if(ds != null){
			  LOGGER.log( Level.FINE, "Closing the datasource");
			  try {
				ds.close();
			}catch (SQLException e) {
				LOGGER.log(Level.OFF, "Failed to clode the data source", e);
			 }
		  }

	  }
	    	
	  	/* Making database connection easier to access from other classes */
		public static Connection getConnection() throws SQLException {
			return getInstance().getDataSource().getConnection();
		}
	
		public void attachShutdownHook(){
			Runtime.getRuntime().addShutdownHook(new Thread( new Runnable() {
				@Override
				public void run() {
				close();
				}
			}));
		}
}

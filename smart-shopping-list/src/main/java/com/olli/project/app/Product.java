package com.olli.project.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Product {
	
	private long id = -1;
	private String name;
	private double amount;
	private double price;
	private long productlist_id = 1;
	
	public long getId() {
		return id;
	}
	public void setId(final long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(final double amount) {
		this.amount = amount;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(final double price) {
		this.price = price;
	}
	
	public long getProductlist_id() {
		return productlist_id;
	}
	public void setProductlist_id(long productlist_id) {
		this.productlist_id = productlist_id;
	}


	//toString method for formatting data
	@Override
	public String toString(){
		final StringBuilder formatted = new StringBuilder();
	
	/*
	try {
		for (int n= 1; n <= ProductHandler.getInstance().getProducts().size(); n++) //Getting the number order of products from size of the products
			if (id == -1){
				formatted.append("[No Id] ");
			}else {
				formatted.append("[").append(n).append("] ");
			}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	*/
		
		if (name == null) {
			formatted.append("no name").append(",");
		} else{
			formatted.append(name).append(",");
		}
		
		
		if (amount <= 0) {
			formatted.append("   ").append("0").append(","); // \t (tabulator) did not work with pane
		} else{

			formatted.append("   ").append(amount).append(" grams").append(",");
		}
		if (price <= 0) {
			formatted.append("   ").append("0");
		} else{
			formatted.append("   ");
			formatted.append(price).append(" euros");
		}
		
		return formatted.toString();
		
	}
	
	//Method for deleting product
	public void deleteProduct() throws SQLException{
	 if (id != -1) {	// if = -1 means that there is no saved product
		final String sql = "DELETE FROM products WHERE id = ?";
		try (Connection connection = DbHandler.getConnection(); PreparedStatement pstmt = connection.prepareStatement(sql)) {	//Making connection
			pstmt.setLong(1,  id);
			pstmt.execute();
			id = -1; //Setting id to -1 like before adding the data
		}
	 }
	}

	
	public void saveProduct() throws SQLException {
		
		try (Connection connection = DbHandler.getConnection()) {	//Making connection
		  if (id == -1) { //Adding values to columns when id = -1
			final String sql = "INSERT INTO products (name, amount, price, productlist_id) VALUES (?, ?, ?, ?)";
				try(PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)){
					
					
					  pstmt.setString(1,  name);
					  pstmt.setDouble(2,  amount);
					  pstmt.setDouble(3,  price);
					  pstmt.setLong(4,  productlist_id);
					  pstmt.execute();
					  
					try (ResultSet rs = pstmt.getGeneratedKeys()) {
						rs.next();
						id =  rs.getLong(1);
						
					}
					 
				}
		  } 
		  else { //Updating columns when id = 1 or greater
			  final String sql = "UPDATE products SET name = ?, amount = ?, price = ? WHERE id = ?";
				try(PreparedStatement pstmt = connection.prepareStatement(sql)){
					
					  pstmt.setString(1,  name);
					  pstmt.setDouble(2,  amount);
					  pstmt.setDouble(3,  price);
					  pstmt.setLong(4, id);
					  pstmt.execute();
		  }
		}
	}
  }


}

package com.olli.project.app;


import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.*;

import javax.swing.SwingUtilities;



public class App {
	
	
	private static Logger LOGGER = Logger.getLogger(App.class.getName());

	public static void main(final String[] args) throws SQLException, IOException {
		DbHandler.getInstance().init();
		DbHandler.getInstance().attachShutdownHook();
		
		
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				LOGGER.log(Level.INFO, "Starting application");
				
				

				
				
				List<Product> products = null;
				try {
					products = ProductHandler.getInstance().getProducts();

				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				/* Starts different view depending on are there any products in database */
				if  (products == null){
					MainFrame main = new MainFrame();
					main.setTitle("Smart Shopping List");
					main.setSize(600, 800); // Setting the resolution for the MainFrame
					main.setLocationRelativeTo(null);
					main.setDefaultCloseOperation(MainFrame.EXIT_ON_CLOSE);
					//main.pack();
					main.setVisible(true); //setting the MainFrame class visible
				}
				else{
					AllListsFrame allListsFrame;
					try {
						allListsFrame = new AllListsFrame();
						allListsFrame.setTitle("Smart Shopping List");
						allListsFrame.setSize(600, 800); // Setting the resolution for the MainFrame
						allListsFrame.setLocationRelativeTo(null);
						allListsFrame.setDefaultCloseOperation(MainFrame.EXIT_ON_CLOSE);
						//allListsFrame.pack();
						allListsFrame.setVisible(true); //setting the MainFrame class visible
					} catch (SQLException e) {
						e.printStackTrace();
					}
				
				}
			}


		});
		LOGGER.log(Level.INFO, "Program executed");
	}
	
	
}

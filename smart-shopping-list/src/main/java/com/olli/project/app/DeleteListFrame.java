package com.olli.project.app;

import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComponent;

import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import javax.swing.SwingUtilities;

public class DeleteListFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3128040097963371829L;
	
	private JLabel deleteLabel;
	private JTextField deleteTextField;
	private JButton deleteBtn;

	JFrame self = this;
	JFrame frame = new JFrame();
	
	/* Constructor */
	public DeleteListFrame() throws SQLException{
		
		createPaneAndContent();
	}
	
	
	
	private JComponent createPaneAndContent() throws SQLException{
		JScrollPane pane = new JScrollPane();
		
		JPanel panel = new JPanel(new GridLayout(3, 0));

		

		//Delete (Lablel)
		deleteLabel = new JLabel("Enter the id of the productlist you want to delete");
		panel.add(deleteLabel);
		
		//Delete (TextField)
		deleteTextField = new JTextField();
		panel.add(deleteTextField);
		
		//Delete (Button)
		deleteBtn = new JButton("Delete");
		panel.add(deleteBtn);

		deleteBtn.addActionListener(new ActionListener() { 
			
		  public void actionPerformed(ActionEvent e) { 
		    try {
				delete(); //Deletes ProductList depending on id
				reloadAllListsFrame();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		  } 
		} );
		
		add(panel);
		
		return pane;
	}
	
	public void delete() throws SQLException{
		
		int input = Integer.parseInt(deleteTextField.getText());
		if (input > 0){
			deleteProductListById(input);
		}
		else{
			JOptionPane.showMessageDialog(self, "Invalid input");
		}
	}
	
	public void reloadAllListsFrame(){
	SwingUtilities.invokeLater( new Runnable(){			
		@Override
		public void run(){
			AllListsFrame allListsFrame;
			try {
				allListsFrame = new AllListsFrame();
				allListsFrame.setSize(600, 800);
				allListsFrame.setLocationRelativeTo(null);
				allListsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				
				/* Destroying all  windows before setting up new*/
				System.gc();
				for (Window window : Window.getWindows()) {
				    window.dispose();
				}
				
				
				allListsFrame.setVisible(true); //setting the AllListsFrame class visible
				
		
				
				

			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	});
	}
	
	public void deleteProductListById(int n) throws SQLException{
		 if (n > 0) {	// if = -1 means that there is no saved product
			final String sql = "DELETE FROM productlist WHERE id = ?";
			try (Connection connection = DbHandler.getConnection(); PreparedStatement pstmt = connection.prepareStatement(sql)) {	//Making connection
				pstmt.setLong(1,  n);
				pstmt.execute();
				
			}

		 }
		 else {
			 System.out.println("DB: Id is not to greater than 0");
		 }
	}
}

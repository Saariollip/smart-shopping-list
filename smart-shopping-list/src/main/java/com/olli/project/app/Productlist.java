package com.olli.project.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ProductList {
	
	private long id = -1;
	private String name;
	private String store;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}

	
	//toString method for formatting data
	@Override
	public String toString(){
		final StringBuilder formatted = new StringBuilder();
		if (id == -1){
			formatted.append("[No Id] ");
		}else {
			formatted.append("[").append(id).append("] ");
		}
		
		if (name == null) {
			formatted.append("no name").append(", ");
		} else{
			formatted.append(name).append(", ");
		}
		
		if (store == null || store == "Optional") {
			formatted.append("no store");
		} else{
			formatted.append(store);
		}
		return formatted.toString();
		
	}
	

	public void saveProductList() throws SQLException {
		
		try (Connection connection = DbHandler.getConnection()) {	//Making connection
		  if (id == -1) { //Adding values to columns when id = -1
			final String sql = "INSERT INTO productlist (name, store) VALUES (?, ?)";
				try(PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)){
					
					
					  pstmt.setString(1,  name);
					  pstmt.setString(2,  store);
					  pstmt.execute();
					  
					try (ResultSet rs = pstmt.getGeneratedKeys()) {
						rs.next();
						id =  rs.getLong(1);
						
					}
					 
				}
		  } 
		  else { //Updating columns when id = 1 or greater
			  final String sql = "UPDATE products SET name = ?, store = ?";
				try(PreparedStatement pstmt = connection.prepareStatement(sql)){
					
					  pstmt.setString(1,  name);
					  pstmt.setString(2,  store);
					  pstmt.setLong(3, id);
					  pstmt.execute();
		  }
		}
	}
  }
	
	public void deleteProductList() throws SQLException{
		 if (id != -1) {	// if = -1 means that there is no saved product
			final String sql = "DELETE FROM productlist WHERE id = ?";
			try (Connection connection = DbHandler.getConnection(); PreparedStatement pstmt = connection.prepareStatement(sql)) {	//Making connection
				pstmt.setLong(1,  id);
				pstmt.execute();
				id = -1; //Setting id to -1 like before adding the data
			}

		 }
	}
	
	public static void deleteProductListById(int n) throws SQLException{
		 if (n > 0) {	// if = -1 means that there is no saved product
			final String sql = "DELETE FROM productlist WHERE id = ?";
			try (Connection connection = DbHandler.getConnection(); PreparedStatement pstmt = connection.prepareStatement(sql)) {	//Making connection
				pstmt.setLong(1,  n);
				pstmt.execute();
				
			}

		 }
		 else {
			 System.out.println("Id have to be greater than 0");
		 }
	}
}

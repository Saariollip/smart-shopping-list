package com.olli.project.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductListHandler {


	private static final ProductListHandler INSTANCE = new ProductListHandler();

	public static ProductListHandler getInstance(){
		return INSTANCE;
	}
	
	private ProductListHandler(){	
	}
	

	
	public List<ProductList> getProductLists() throws SQLException{
		List<ProductList> productLists = new ArrayList<>();
		
		String query = "SELECT * FROM productlist ORDER BY id";
		try(Connection connection = DbHandler.getConnection();
				PreparedStatement psmt = connection.prepareStatement(query);
				ResultSet rs = psmt.executeQuery()){
			
			while(rs.next()){
				ProductList pl = new ProductList();
				pl.setId(rs.getLong("id"));
				pl.setName(rs.getString("name"));
				pl.setStore(rs.getString("store"));
				productLists.add(pl);
			}
		}
		
 		return productLists;
	}
	

}

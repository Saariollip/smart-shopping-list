package com.olli.project.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import javax.swing.SwingUtilities;

import javax.swing.table.DefaultTableModel;


public class AllListsFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2657232176479602999L;

	
	/* Labels */
	private JLabel showLabel;



	
	/* Table */
	private JTable table;

	
	/* Buttons */
	private JButton deleteBtn;
	private JButton newListBtn;
	
	
	
	/* Actions */
	private ActionListener deleteListAction;
	private ActionListener newListAction;
	

	
	/* For managing frame (class) */
	JFrame self = this;

	
	
	/* Constructor */
	public AllListsFrame() throws SQLException{
		
		initActions();
		addComponents();
	

		
	}

	

	private JComponent createButtons(){		
		final JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 2));
		panel.setMaximumSize(new Dimension( 100, 100));
		Dimension btnSize = new Dimension(50, 50);
		

		deleteBtn = new JButton("Delete List", load("Delete"));
		deleteBtn.addActionListener(deleteListAction);
		deleteBtn.setPreferredSize(btnSize);
		panel.add(deleteBtn);
		
		newListBtn = new JButton("New List", load("New"));
		newListBtn.addActionListener(newListAction);
		newListBtn.setPreferredSize(btnSize);
		panel.add(newListBtn);
		
		
		return panel;
	}

	private JComponent createShowLabel(){		
		final JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 1));
		panel.setMaximumSize(new Dimension( 50, 50));

		

		showLabel = new JLabel("Click id of the list to show its content");
		panel.add(showLabel);
		
	
		return panel;
	}

	private void deleteList() throws SQLException{
		
		SwingUtilities.invokeLater( new Runnable(){			
			@Override
			public void run(){
				DeleteListFrame deleteListFrame;
				try {
					deleteListFrame = new DeleteListFrame();
					deleteListFrame.setSize(300, 200);
					deleteListFrame.setLocationRelativeTo(null);
					deleteListFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					deleteListFrame.setVisible(true); //setting the MainFrame class visible
				} catch (SQLException e) {
					e.printStackTrace();
				}

		

			}
		});

		

	}
	
	/* Method for loading icons */
	private ImageIcon load(final String name) {
		return new ImageIcon(getClass().getResource("/icons/" + name + ".png"));
	}

	private JComponent createTablePane() throws SQLException{
		
		Connection connection = DbHandler.getConnection();
		Statement st = connection.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM productlist");
		

		table = new JTable(buildTableModel(rs));
		 
		table.setPreferredScrollableViewportSize(new Dimension(300, 20));
		table.setFillsViewportHeight(true);
		JScrollPane pane = new JScrollPane(table);
		
		/* Mouse action for table */
		table.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = table.rowAtPoint(evt.getPoint());
		    
		        if (row >= 0) {
		        	long listId = (long) table.getModel().getValueAt(row, 0); //Getting object from the cell and changing it to integer
		        	showListContent(listId);			        	
		        }
		    }
		});

		
		  return (pane);
	}
	

	



	
	private void initActions(){
		

		deleteListAction = new ActionListener(){
			@Override
			public void actionPerformed(final ActionEvent ae) {
				try {
					deleteList();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		 };
		 
		 newListAction = new ActionListener(){
			@Override
			public void actionPerformed(final ActionEvent ae) {
				switchViewToMainFrame();
			}
		 };
		 

	
	}
	

	private void switchViewToMainFrame(){
		

		SwingUtilities.invokeLater( new Runnable(){			
			@Override
			public void run(){
				MainFrame mainFrame;
				try {
					mainFrame = new MainFrame(getNewProductListId());
					mainFrame.setSize(600, 800);
					mainFrame.setLocationRelativeTo(null);
					mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					mainFrame.setVisible(true); //setting the MainFrame class visible
					self.setVisible(false); // Hiding the SaveListFrame view
					
				} catch (SQLException e) {				
				  e.printStackTrace();
				}


			}
		});

	}
	
	public void showListContent(final long listId){
		SwingUtilities.invokeLater( new Runnable(){			
			@Override
			public void run(){
				MainFrame mainFrame;

					mainFrame = new MainFrame(listId);
					mainFrame.setSize(600, 800);
					mainFrame.setLocationRelativeTo(null);
					mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					mainFrame.setVisible(true); //setting the MainFrame class visible
					self.setVisible(false); // Hiding the SaveListFrame view
					


			}
		});
	}
	
	
	public long getNewProductListId() throws SQLException{
		long productListId = 0;
		
		Connection connection = DbHandler.getConnection();
		Statement st = connection.createStatement();
		ResultSet rs_max_id = st.executeQuery("SELECT id FROM productlist ORDER BY id DESC LIMIT 1");
		
		while (rs_max_id.next()){
			productListId = rs_max_id.getLong(1); // Raising it by one because new list
		}

		productListId++;

		
		return productListId;
	}
	
	public static DefaultTableModel buildTableModel(ResultSet rs)
	        throws SQLException {

	    ResultSetMetaData metaData = rs.getMetaData();

	    // names of columns from DB
	    Vector<String> columnNames = new Vector<String>();
	    int columnCount = metaData.getColumnCount();
	    for (int column = 1; column <= columnCount; column++) {
	        columnNames.add(metaData.getColumnName(column));
	    }
	    columnNames.add("# OF PRODUCTS");
	    columnNames.add("TOTAL PRICE");
	    
	    ResultSet rs_count = getProductDetails();
	    ResultSetMetaData md = rs_count.getMetaData();
	    
	    int colCount = md.getColumnCount();
	 
	    

	       
	       
	    // data of the table from DB
	    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
	    
	      /* Adding First three columns' data from database */
		    while(rs.next()) {
		    	Vector<Object> vector = new Vector<Object>();
		        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
		            vector.add(rs.getObject(columnIndex));

	            }
		        
		       /* Inserting data for # OF PRODUCTS and TOTAL PRICE columns if they exist */
		       if (rs_count.next())
		        {
		        	 for (int columnIndex = 1; columnIndex <= colCount; columnIndex++) {
				            vector.add(rs_count.getObject(columnIndex));

			            }
		       
	        }	    
		        data.add(vector);	        	 
	        	
	        }

		   
		    
	    return new DefaultTableModel(data, columnNames);

	}
	
	/* For Getting number of products and sum of the products' price */
	public static ResultSet getProductDetails() throws SQLException{
		Connection connection = DbHandler.getConnection();
		Statement st = connection.createStatement();
		ResultSet rs_count = st.executeQuery("SELECT count(name), sum(price) FROM products GROUP BY productlist_id");
		
	
		return rs_count;	
	}


	/* Adding GUI Components to the BorderLayout */
	private void addComponents() throws SQLException{
		
		add(createTablePane(), BorderLayout.CENTER);
		add(createButtons(), BorderLayout.PAGE_START);
		add(createShowLabel(), BorderLayout.PAGE_END);
	}
}

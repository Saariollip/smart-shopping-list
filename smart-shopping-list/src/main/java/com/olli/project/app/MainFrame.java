package com.olli.project.app;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6361100795391863490L; //Generated

	
	/* Labels */
	private JLabel productLabel;
	private JLabel nameLabel;
	private JLabel amountLabel;
	private JLabel priceLabel;
	
	/* Textfields */
	private JTextField nameTextField;
	private JTextField amountTextField;
	private JTextField priceTextField;
	
	
	/* Productlist for handling multiple products */
	private DefaultListModel<Product> productsListModel;
	private JList<Product> productsList;
	
	/* Actions */
	private Action backAction;
	private Action saveAction;
	private Action deleteAction;
	private Action saveListAction;
	
	/* Objects from another classes */
	private Product selectedProduct = new Product();

	
	/* For Handling productlist and products relationship */
	long listId;
	
	/* For managing frame */ 
	JFrame self = this;
	

	/* Default Constructor */
	public MainFrame(){

		createListPane();
		initActions(); //Initialising actions
		initComponents(); //Initialising components
		
		refreshData(); //For updating database content to the view
		createNew();
	}
	
	/* Constructor with parameter */
	public MainFrame(long productListId)
	{

		setProductListId(productListId);

		initActions(); //Initialising actions
		initComponents(); //Initialising components
		
		refreshData(); //For updating database content to the view
		createNew(); //For setting up textfields
	}
	
	public void setProductListId(long productListId){
		listId = productListId;
	}

	/* Creating toolbar where all the actions are placed */
	private JToolBar createToolBar() {
		final JToolBar tb = new JToolBar();
		tb.setRollover(true);
		tb.setToolTipText("Methods from left to right: Back, Save list"); //Should have used buttons with actionListeners instead of actions
		
	
		tb.add(backAction);
		
		for (int i=0; i<15; i++) 
		{
		tb.addSeparator();
		}
	
		tb.add(saveAction);
		
		tb.addSeparator();
		
		tb.add(deleteAction);
		for (int i=0; i<15; i++) 
		{
		tb.addSeparator();
		}
		tb.add(saveListAction);
		
		return tb;
	}
	
	/* Creating the Form for adding new products */
	private JComponent createForm() {
		final JPanel panel = new JPanel(new GridLayout(10, 4));
		
		//New product label
		productLabel = new JLabel("New Product:");
		panel.add(productLabel);

		//Name (Label)
		nameLabel = new JLabel("Name:");
		panel.add(nameLabel);
		
		//Name (Textfield)
		nameTextField = new JTextField();
		panel.add(nameTextField);
		
		//Amount (Label)
		amountLabel = new JLabel("Amount: (grams)");
		panel.add(amountLabel);
		
		//Amount (Textfield)
		amountTextField = new JTextField();
		panel.add(amountTextField);
		
		//Price (Label)
		priceLabel = new JLabel("Price: (euros)");
		panel.add(priceLabel);
		
		//Price(Textfield)
		priceTextField = new JTextField();
		panel.add(priceTextField);

		return panel;
	}

	/* Method for loading icons */
	private ImageIcon load(final String name) {
		return new ImageIcon(getClass().getResource("/icons/" + name + ".png"));
	}



	private void refreshData(){	
		productsListModel.removeAllElements(); //Avoiding adding same elements all over again when pressing refresh button multiple times

			final SwingWorker<Void, Product> w = new SwingWorker<Void, Product>(){
	
				@Override
				protected Void doInBackground() throws Exception {
					List<Product> products = ProductHandler.getInstance().getProductsById(listId);
					for( Product product : products){
						publish(product); //Move this product to event thread
				  }
					return null;
				}
				
				@Override
				protected void process(List<Product> products){
					for( Product product : products){
						productsListModel.addElement(product); //Copies product object thread from worker thread to the event dispatcher thread
				  }
				}
			};
				w.execute();
		
		
	}

	
	/* Method for saving the product */
	private void save(){
		
		if(selectedProduct != null ) {
			
		selectedProduct.setName(nameTextField.getText());
		selectedProduct.setAmount(Double.parseDouble(amountTextField.getText()));
		
		selectedProduct.setPrice(Double.parseDouble(priceTextField.getText()));
		selectedProduct.setProductlist_id(listId);
		
			try {
				selectedProduct.saveProduct();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(this, "Failed to save the selected product", "Save", JOptionPane.WARNING_MESSAGE);
			} finally{
				refreshData(); //Making saved product to show in the list pane
			}
		
		}
		else
		{
			JOptionPane.showMessageDialog(self, "Press + icon to create new products");
		}
	}
	

	private void initActions(){
		
		backAction = new AbstractAction("Back", load("Back")) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -7914420718581478824L;

			@Override
			public void actionPerformed(final ActionEvent ae) {
					switchViewToAllListsFrame();

			}
		};
			
		

		
		 saveAction = new AbstractAction("Save", load("Save2")){
						
			/**
			 * 
			 */
			private static final long serialVersionUID = -1635198372859162815L;

			@Override
			public void actionPerformed(final ActionEvent e) {

				 save();
				 createNew();
			 
				
			}
		};
		
		 deleteAction = new AbstractAction("Delete", load("Delete")){
					
			/**
			 * 
			 */
			private static final long serialVersionUID = 950093096278839577L;

			@Override
			public void actionPerformed(final ActionEvent e) {
				delete();
			}
		};
		saveListAction = new AbstractAction("saveList", load("saveList")){
			
			

			/**
			 * 
			 */
			private static final long serialVersionUID = 2587457832789485716L;

			@Override
			public void actionPerformed(final ActionEvent e) {
				
			   
			   eraseDataFromListPane();
				switchViewToSaveListFrame();
		
			}
		};
	}
	
	private void eraseDataFromListPane(){
		productsListModel.removeAllElements(); //Avoiding adding same elements all over again when pressing refresh button multiple times
	}
	
	
	private void switchViewToSaveListFrame(){
		SwingUtilities.invokeLater( new Runnable(){			
			@Override
			public void run(){
				SaveListFrame saveListFrame = new SaveListFrame();
				saveListFrame.setSize(600, 800);
				saveListFrame.setLocationRelativeTo(null);
				saveListFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				saveListFrame.setVisible(true); //setting the SaveListFrame class visible
				self.setVisible(false); // Hiding the MainFrame view

			}
		});

	}
	
	private void switchViewToAllListsFrame(){
		SwingUtilities.invokeLater( new Runnable(){			
			@Override
			public void run(){
				AllListsFrame allListsFrame;
				try {
					allListsFrame = new AllListsFrame();
					allListsFrame.setSize(600, 800);
					allListsFrame.setLocationRelativeTo(null);
				    allListsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					allListsFrame.setVisible(true); //setting the SaveListFrame class visible
				    self.setVisible(false); // Hiding the MainFrame view
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		});

	}
	
	private JComponent createListPane(){
		  productsListModel = new DefaultListModel<>();
		  productsList = new JList<>(productsListModel);
		  productsList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			 @Override
			 public void valueChanged(ListSelectionEvent e){
				 if(!e.getValueIsAdjusting()){
					 Product p = productsList.getSelectedValue();
					 setSelectedProduct(p);
				 }
			 }
		  });

		  return new JScrollPane(productsList);
	}
	

	private void setSelectedProduct(Product product){
		this.selectedProduct = product;
		if(product == null ){ //avoiding nullpointerException
			
		//	idTextField.setText("");
			nameTextField.setText("");
			amountTextField.setText("");
			priceTextField.setText("");
		}
		else{
		//	idTextField.setText( String.valueOf(product.getId()) );
			nameTextField.setText(product.getName());
			amountTextField.setText( String.valueOf(product.getAmount()) );
			priceTextField.setText( String.valueOf(product.getPrice()) );
		}
	}
	
	private void createNew(){
		Product product = new Product();
		product.setName("Required");
		product.setAmount(0);
		product.setPrice(0);
		setSelectedProduct(product);
		
	}

	private void delete(){
		if (selectedProduct!= null) {
			if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(this,  "Are you sure you want to delete" + selectedProduct + "?", "Delete", JOptionPane.YES_NO_OPTION)){
				try{
					selectedProduct.deleteProduct();
				} catch (final SQLException e) {
					JOptionPane.showMessageDialog(this,  "Failed to delete the selected product", "Delete", JOptionPane.WARNING_MESSAGE);
				} finally{
					setSelectedProduct(null);
					refreshData(); //Making deleted product disappear in list pane
				}
			}
		}
	}
	
	
	private void initComponents(){
		add(createToolBar(), BorderLayout.PAGE_START);
		add(createListPane(), BorderLayout.PAGE_END);
		add(createForm(), BorderLayout.CENTER);
		
	}
	

	
	
}

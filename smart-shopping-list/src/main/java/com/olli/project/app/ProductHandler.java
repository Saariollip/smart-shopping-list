package com.olli.project.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductHandler {

	private static final ProductHandler INSTANCE = new ProductHandler();

	public static ProductHandler getInstance(){
		return INSTANCE;
	}
	
	private ProductHandler(){	
	}
	
	public List<Product> getProducts() throws SQLException{
		List<Product> products = new ArrayList<>();
		
		String query = "SELECT * FROM products ORDER BY productlist_id DESC LIMIT 1";
		try(Connection connection = DbHandler.getConnection();
				PreparedStatement psmt = connection.prepareStatement(query);
				ResultSet rs = psmt.executeQuery()){
			
			while(rs.next()){
				Product p = new Product();
				p.setId(rs.getLong("id"));
				p.setName(rs.getString("name"));
				p.setAmount(rs.getDouble("amount"));
				p.setPrice(rs.getDouble("price"));
				products.add(p);
			}
		}
		
 		return products;
	}
	public List<Product> getProductsById(long id) throws SQLException{
		List<Product> products = new ArrayList<>();
		
		String query = "SELECT * FROM products WHERE productlist_id=" + id;
		try(Connection connection = DbHandler.getConnection();
				PreparedStatement psmt = connection.prepareStatement(query);
				ResultSet rs = psmt.executeQuery()){
			
			while(rs.next()){
				Product p = new Product();
				p.setId(rs.getLong("id"));
				p.setName(rs.getString("name"));
				p.setAmount(rs.getDouble("amount"));
				p.setPrice(rs.getDouble("price"));
				products.add(p);
			}
		}
		
 		return products;
	}
	public List<Product> getAllProducts() throws SQLException{
		List<Product> products = new ArrayList<>();
		
		String query = "SELECT * FROM products ORDER BY id";
		try(Connection connection = DbHandler.getConnection();
				PreparedStatement psmt = connection.prepareStatement(query);
				ResultSet rs = psmt.executeQuery()){
			
			while(rs.next()){
				Product p = new Product();
				p.setId(rs.getLong("id"));
				p.setName(rs.getString("name"));
				p.setAmount(rs.getDouble("amount"));
				p.setPrice(rs.getDouble("price"));
				products.add(p);
			}
		}
		
 		return products;
	}
	
}

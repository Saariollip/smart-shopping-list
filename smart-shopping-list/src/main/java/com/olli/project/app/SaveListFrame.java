package com.olli.project.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

public class SaveListFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 778835202895572622L;
	
	
	/* Labels */
	private JLabel listLabel;
	private JLabel nameLabel;
	private JLabel storeLabel;

	
	/* Textfields */
	private JTextField idTextField;
	private JTextField nameTextField;
	private JTextField storeTextField;

	
	/* Buttons */
	private JButton saveBtn;
	private JButton cancelBtn;
	
	
	/* ActionListeners */
	private ActionListener saveAction;
	private ActionListener cancelAction;


	
	
	/* Objects from the other classes */
	private ProductList selectedProductList;
	
	/* For managing frame (class) */
	JFrame self = this;
	
	
	/* Constructor */
	public SaveListFrame(){
		initActions();
		addComponents();
		initTextFields();
	}
	
	private JComponent createListForm() {
		final JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(10, 4));
		panel.setMaximumSize(new Dimension(200, 150));
		
		
		listLabel = new JLabel("Name and set store for your list");
		
		// Id (Textfield)
		idTextField = new JTextField();
		idTextField.setVisible(false); //Set true for debugging purposes 
		panel.add(idTextField);
		
		
	
		panel.add(listLabel);
		//Name (Label)
		nameLabel = new JLabel("Name:");
		panel.add(nameLabel);
		
		//Name (Textfield)
		nameTextField = new JTextField();
		panel.add(nameTextField);	
		
		//Name (Label)
		storeLabel = new JLabel("Store:");
		panel.add(storeLabel);
		
		//Name (Textfield)
		storeTextField = new JTextField();
		panel.add(storeTextField);
		
		
		
	
		
		
		return panel;
	}
	
	private JComponent createButtons(){
		
		final JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 2));
		panel.setMaximumSize(new Dimension( 100, 100));
		Dimension btnSize = new Dimension(50, 50);
		
		saveBtn = new JButton("Save list", load("Save"));
		saveBtn.addActionListener(saveAction);
		saveBtn.setPreferredSize(btnSize);
		panel.add(saveBtn);
		
		
		cancelBtn = new JButton("Cancel", load("Cancel"));
		cancelBtn.addActionListener(cancelAction);
		cancelBtn.setPreferredSize(btnSize);
		panel.add(cancelBtn);
		
		
		return panel;
	}
	
	/* Method for loading icons */
	private ImageIcon load(final String name) {
		return new ImageIcon(getClass().getResource("/icons/" + name + ".png"));
	}
	
	private void saveList(){
		if(selectedProductList != null ) {
			
		selectedProductList.setName(nameTextField.getText());
		selectedProductList.setStore(storeTextField.getText());

			try {
				selectedProductList.saveProductList();
			} catch (SQLException e) {
				JOptionPane.showMessageDialog(idTextField, e);
			} finally{
				refreshData();
			}
		
		}
		else{
			System.out.println("Productlist is null");
		}

	}
	private void refreshData(){	
		//productsListsModel.removeAllElements(); //Avoiding adding same elements all over again when pressing refresh button multiple times
		final SwingWorker<Void, ProductList> w = new SwingWorker<Void, ProductList>(){

			@Override
			protected Void doInBackground() throws Exception {
				List<ProductList> productLists = ProductListHandler.getInstance().getProductLists();
				for( ProductList productList : productLists){
					publish(productList); //Move this product to event thread
			  }
				return null;
			}
			

		};
			w.execute();
	}
	
	private void setSelectedProductList(ProductList productList){
		this.selectedProductList = productList;
		if(productList == null ){ //avoiding nullpointerException
			idTextField.setText("");
			nameTextField.setText("");
			storeTextField.setText("");

		}
		else{
			idTextField.setText( String.valueOf(productList.getId()) );
			nameTextField.setText(productList.getName());
			storeTextField.setText( productList.getStore() );

		}
	}
	
	private void initTextFields(){
		ProductList pl = new ProductList();
		pl.setName("Required");
		pl.setStore("(Optional)");
		setSelectedProductList(pl);
	}
	
	private void initActions(){
		
		 saveAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
			saveList();
			switchViewToAllListsFrame();	
			}
		 };
		
		 cancelAction = new ActionListener(){
			@Override
			public void actionPerformed(final ActionEvent ae) {
				cancel();
			}
		 };
	}
	
	private void switchViewToAllListsFrame(){
		SwingUtilities.invokeLater( new Runnable(){			
			@Override
			public void run(){
				AllListsFrame allListsFrame;
				try {
					allListsFrame = new AllListsFrame();
					allListsFrame.setSize(600, 800);
					allListsFrame.setLocationRelativeTo(null);
					allListsFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					allListsFrame.setVisible(true); //setting the AllListsFrame class visible
					self.setVisible(false); // Hiding the saveListFrame view

				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		});
	}
	private void cancel(){
		SwingUtilities.invokeLater( new Runnable(){			
			@Override
			public void run(){
				MainFrame mainFrame = new MainFrame();
				mainFrame.setSize(600, 800);
				mainFrame.setLocationRelativeTo(null);
				mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				mainFrame.setVisible(true); //setting the MainFrame class visible
				self.setVisible(false); // Hiding the SaveListFrame view

			}
		});

	}
	private void addComponents(){
		add(createListForm(), BorderLayout.CENTER);
		add(createButtons(), BorderLayout.PAGE_END);
	}
}

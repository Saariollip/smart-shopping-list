/* Adding some test products to database */
INSERT INTO products (name, amount, price, productlist_id) VALUES ('Porkkana', 5, 10, 1);
INSERT INTO products (name, amount, price, productlist_id) VALUES ('Peruna', 12, 18, 1);
INSERT INTO products (name, amount, price, productlist_id) VALUES ('Herkkusieni', 4, 6, 1);

/*Adding test productlist */
INSERT INTO productlist (name, store) VALUES ('Herkkulista', 'Lidl');
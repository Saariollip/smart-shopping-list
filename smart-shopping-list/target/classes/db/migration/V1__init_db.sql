/* Creating the productlist table */
CREATE TABLE productlist(
	id bigint auto_increment NOT NULL,
	name varchar(128) NOT NULL,
	store varchar(128),
	PRIMARY KEY(id)
);

/* Create the products table */
CREATE TABLE products(
	id bigint auto_increment NOT NULL,
	name varchar(128) NOT NULL,
	amount decimal(12, 2),
	price decimal (12, 2),
	productlist_id bigint,
	PRIMARY KEY(id)
);
